//- this should not be necessary, included in ClientSocket or deeper includes
//- #include <yat/network/Address.h>
#include <yat/network/ClientSocket.h>

#ifndef __PIE712_H__
#define __PIE712_H__

//TODO TODO 
//- TODO in PIE712.cpp do not forget to initialize singleton to 0 by adding this line outside of class: 
//- PIE712 * PIE712::singleton = 0;

class PIE712
{

public:
	//- the configuration structure
	typedef struct Config
	{
		//- members
		std::string url;
		size_t port;

		//- Ctor -------------
		Config ();
		//- Ctor -------------
		Config ( const Config & _src);
		//- Ctor -------------
		Config (std::string url );

		//- operator = -------
		void operator = (const Config & src);
	}Config;

//---------------------------------------------------
//- the singleton stuff
//---------------------------------------------------
private :
	//- class Constructor
	PIE712 (const Config & conf); /// Connect on controller
	//- class Destructor
	virtual ~PIE712();
	static PIE712 * singleton;

public:
//---------------------------------------------------
	//- creates really the PIE712 class
	static void initialize (Config & conf) throw (yat::Exception);
	static void release (void);
	static PIE712 & instance (void) throw (yat::Exception);


//-----------------------------------------------------
//- some enums and constants
//-----------------------------------------------------
  typedef enum
	{
		CONTROLLER_NOT_CONNECTED,			/// Connect() has not been called, E712 controller was not found or Disconnect() has been called
		CONTROLLER_CONNECTED,				/// TCP connection done
		CONTROLLER_CONNECTION_ERROR,		/// Failed to connect to PI E712
		CONTROLLER_NOT_REFERENCED,			/// at least one axis is connected but not homed/referenced
		CONTROLLER_REFERENCING,				/// at least one axis performing homing /referencing motions
		CONTROLLER_STANDBY,					/// all axes are initialized and ready for motion commands
		CONTROLLER_MOVING,					/// at least one axis on its way to the new target position
		CONTROLLER_WAVE_GENERATOR_RUNNING,	/// at least one axis is running the wave generator. 
		CONTROLLER_SERVO_OFF,       
		CONTROLLER_MOTION_ERROR,
		CONTROLLER_HEXPOD_ERROR,
		CONTROLLER_NO_STATE
	} E712_CONTROLLER_STATE;

	typedef enum
	{
		AXIS_NOT_REFERENCED,			/// axis is not homed/referenced
		AXIS_STANDBY,					/// axis initialized and ready for motion commands
		AXIS_MOVING,					/// axis is on its way to the new target position
		AXIS_WAVE_GENERATOR_RUNNING,	/// axis is running the wave generator. 
		AXIS_SERVO_OFF,
		AXIS_NO_STATE
	} E712_AXIS_STATE;

	typedef enum
	{
		HOMING_NO_HOMING = 1,
		HOMING_TO_NEGATIVE_LIMIT = 2,
		HOMING_TO_POSITIVE_LIMIT = 3,
		HOMING_TO_SIGNED_RFERENCE = 4,
		HOMING_TO_REFERENCE_IMPULSE = 5
	} E712_HOMING_MODE;

	enum
	{
		E712_MOTION_ERROR = 0x00003F,
		E712_MOVING = 0x003F00,
		E712_REFERENCING = 0x080000,
		E712_READY = 0x010000
	};
//---------------------------------------------------

	E712_CONTROLLER_STATE GetStatus(); /// get state of the controller
	E712_AXIS_STATE GetAxisStatusOfAxis(unsigned int controllerAxisId); /// get state of controller axis 'controllerAxisId'
	void GetAxisStatus(std::vector<E712_AXIS_STATE>& axisState);/// get the state of all controller axes.
	int GetNumberOfAxes(); // gets the number of axes which are connected to the controller.

	void StopAxis(unsigned int controllerAxisId); /// stop motion for axis 'controllerAxisId'
	void Stop(); /// stop anything, reference, normal motion,..., for all controller axes

	void SetHomingModeOfAxis(unsigned int controllerAxisId, E712_HOMING_MODE homingMode);/// set homing mode of axis 'controllerAxisId' to target position
	void SetHomingMode(const std::vector<E712_HOMING_MODE>& homingMode);/// set homing mode of all controller axes to target position

	void SetHomingPositionOfAxis(unsigned int controllerAxisId, double homingPosition);/// set the homing position of axis 'controllerAxisId'
	void SetHomingPosition(const std::vector<double>& homingPosition);/// set the homing position of all controller axes 

	void HomeAxis(unsigned int controllerAxisId); /// start referencing of 'unsigned int controllerAxisId'
	void Home(); /// start referencing of all controller axes

	double GetPositionOfAxis(unsigned int controllerAxisId); /// current position of Axis 'controllerAxisId'
	void GetPosition(std::vector<double>& currentPosition); /// get current positions of all controller axes

	void MoveAbsoluteAxis(unsigned int controllerAxisId, double targetPosition);/// move axis 'controllerAxisId' to target position
	void MoveAbsolute(const std::vector<double>& targetPosition);/// move all controller axes to target position

	void MoveReletiveAxis(unsigned int controllerAxisId, double relativePosition);/// move axis 'controllerAxisId' relative
	void MoveReletive(const std::vector<double>& relativePosition);/// move all controller axes relative

	double GetTargetOfAxis(unsigned int controllerAxisId); /// current target of axis 'controllerAxisId'
	void GetTarget(std::vector<double>& targetPotition); /// get current target of all controller axes

	void SetVelocityOfAxis(unsigned int controllerAxisId, double velocity); /// set velocity of axis 'controllerAxisId'
	void SetVelocity(const std::vector<double>& velocity);/// set velocity of all controller axes

	double GetVelocityOfAxis(unsigned int controllerAxisId); /// get velocity of axis 'controllerAxisId'
	void GetVelocity(std::vector<double>& velocity);/// get velocity of all controller axes

	void EnableServoOfAxis(unsigned int controllerAxisId); /// enable the servo for axis 'controllerAxisId'
	void EnableServo();/// enable the servo for all controller axes

	void DisableServoOfAxis(unsigned int controllerAxisId); /// disable the servo for axis 'controllerAxisId'
	void DisableServo();/// disable the servo for all controller axes

	double GetSoftPositiveLimitOfAxis(unsigned int controllerAxisId); /// get current positive soft limit for axis 'controllerAxisId'
	double GetSoftNegativeLimitOfAxis(unsigned int controllerAxisId); /// get current negative soft limit for axis 'controllerAxisId'
	void GetSoftLimits(std::vector<double>& positiveLimits, std::vector<double>& negativeLimits); ///< get current soft limits for all controller axes

	void SetSoftPositiveLimitOfAxis(unsigned int controllerAxisId, double positiveLimits); /// set current positive soft limit for axis 'controllerAxisId'
	void SetSoftNegativeLimitOfAxis(unsigned int controllerAxisId, double negativeLimits); /// set current negative soft limit for axis 'controllerAxisId'
	void SetSoftLimits(const std::vector<double>& positiveLimits, const std::vector<double>& negativeLimits); /// set current soft limits for all controller axes

	void SendGCSCommand(const std::string& command);/// send GCS commmand to controller 0
	std::string ReadGCSAnswer();/// read answer from Controller 0



protected:

	typedef struct piE712Axis_
	{
		E712_AXIS_STATE axisState;
		unsigned int	axisName;
	}PI_E712_AXIS;

	typedef struct piE712Controller_
	{
		yat::ClientSocket		controllerSocket;
		E712_CONTROLLER_STATE	controllerState;
		int						lastError;
		std::vector<PI_E712_AXIS*> controllerAxis;
	}PI_E712_CONTROLLER;

	typedef struct piSystemAxis
	{
		int	idController;
		int idAxis;
	}PI_SYSTEM_AXIS;

	std::vector<PI_SYSTEM_AXIS*> m_controllerAxis;

	std::vector<PI_E712_CONTROLLER*> m_Controller;
	void Connect(const std::vector<Config>& conf); 
	void Disconnect();
	void ReadStateFromE712(int iE712Id);
	void InitSocket(int iE712Id);
	int ReadError(int iE712Id);
	void CheckConnection(int iE712Id, const char* funcname)
	{
		if (m_Controller[iE712Id]->controllerState == CONTROLLER_NOT_CONNECTED || m_Controller[iE712Id]->controllerState == CONTROLLER_CONNECTION_ERROR)
		{
			throw yat::Exception(	"SOFTWARE_ERROR", 
				                    "Connection with E712 not opened or broken",
								    funcname);
		}
	}
	void CheckBusy(int iE712Id, const char* funcname)
	{
		if (m_Controller[iE712Id]->controllerState == CONTROLLER_MOVING || m_Controller[iE712Id]->controllerState == CONTROLLER_REFERENCING)
		{
			ReadStateFromE712(iE712Id);
			if (m_Controller[iE712Id]->controllerState == CONTROLLER_MOVING || m_Controller[iE712Id]->controllerState == CONTROLLER_REFERENCING)
			{
				throw yat::Exception(	"OPERATION_NOT_ALLOWED", 
					                    "E712 is already homing/moving",
									    funcname);
			}
		}
	}
	void CheckStandby(int iE712Id, const char* funcname)
	{
		if (m_Controller[iE712Id]->controllerState != CONTROLLER_STANDBY)
		{
			ReadStateFromE712(iE712Id);
			if (m_Controller[iE712Id]->controllerState != CONTROLLER_STANDBY)
			{
				throw yat::Exception(	"OPERATION_NOT_ALLOWED", 
					                    "E712 is not ready for motion",
									    funcname);
			}
		}
	}
	void ThrowE712Error(int iE712Id, const std::string& description, const std::string& funcname);
	void CheckE712ForError(int iE712Id, const std::string& description, const std::string& funcname);
	void ThrowSoftwareError(const std::string& description, const std::string& funcname);
	void ReadFloatValues(int iE712Id, std::vector<double>& positions);
	void ReadSingleFloatValue(int iE712Id, std::vector<double>& positions);
	void ReadIntValues(int iE712Id, std::vector<int>& values);
	double ReadSinglePosition(int iE712Id, unsigned int controllerAxisName);
	double ReadSingleTarget(int iE712Id, unsigned int controllerAxisName);
	double ReadSinglePositiveLimit(int iE712Id, unsigned int controllerAxisName);
	double ReadSingleNegativeLimit(int iE712Id, unsigned int controllerAxisName);
	void MoveAbsoluteSingleAxis(int iE712Id, unsigned int controllerAxisName, double targetPosition);
	void MoveRelativeSingleAxis(int iE712Id, unsigned int controllerAxisName, double targetPosition);
	int GetCommandLevel(int iE712Id);
	void SetCommandLevel(int iE712Id, int commandLevel);
};

#endif //- __PIE712_H__

