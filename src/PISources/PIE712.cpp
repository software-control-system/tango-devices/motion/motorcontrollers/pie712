#include "PIE712.h"

#include <iostream>
#include <numeric>
#include <stdlib.h>

using namespace std;

#ifndef _MSC_VER
#define __FUNCTION__ __func__
#endif

PIE712 * PIE712::singleton = NULL;

void PIE712::initialize (Config & conf) throw (yat::Exception)
{
	//- already created!
	if (singleton)
		return;

	try
	{
		singleton = new PIE712 (conf);
		if (singleton == 0)
			throw std::bad_alloc ();
	}
	catch (...)
	{
		THROW_YAT_ERROR("SOFTWARE_ERROR",
                     "unable to create instance of PIE712",
                     "PIE712::initialize"); 
	}
}

void PIE712::release (void)
{
	if(singleton)
		delete singleton;

	singleton = 0;

}

PIE712 & PIE712::instance (void) throw (yat::Exception)
{
	if (!singleton)
		THROW_YAT_ERROR("SOFTWARE_ERROR",
                       "unexpected NULL pointer [PIE712 singleton not properly initialized]",
                       "PIE712::instance"); 
	return *singleton;
}

// ============================================================================
// Config::Config
// ============================================================================
PIE712::Config::Config ()
{
	url		= "Not Initialised";
	port	= 50000;
}

PIE712::Config::Config (const Config & _src)
{
	*this = _src;
}
// ============================================================================
// DBConfig::operator =
// ============================================================================
void PIE712::Config::operator = (const Config & _src)
{
	url		= _src.url;
	port	= _src.port;
}


// ============================================================================
// CTor
// ============================================================================
PIE712::PIE712(const Config & _conf)
{
	std::vector<Config> tmpConf; 
	tmpConf.push_back(_conf);

	try 
	{
		this->Disconnect();
		this->Connect(tmpConf);
	}
	catch (const yat::SocketException & e)
	{
		if(m_Controller.size() > 0)
			this->m_Controller[m_Controller.size() - 1]->controllerState = CONTROLLER_CONNECTION_ERROR;

		return;
	} 
}

// ============================================================================
// DTor
// ============================================================================
PIE712::~PIE712 ()
{
	this->Disconnect();
}

// ============================================================================
// Init Socket Options
// ============================================================================
void PIE712::InitSocket(int iE712Id)
{
	m_Controller[iE712Id]->controllerSocket.set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
	m_Controller[iE712Id]->controllerSocket.set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
	m_Controller[iE712Id]->controllerSocket.set_option(yat::Socket::SOCK_OPT_OTIMEOUT, 10000);
	m_Controller[iE712Id]->controllerSocket.set_option(yat::Socket::SOCK_OPT_ITIMEOUT, 10000);
}

// ============================================================================
// Socket connect
// ============================================================================
void PIE712::Connect(const std::vector<Config>& conf)
{
	int i;

	for(i = 0; i < int(conf.size()); i++)
	{
		PI_E712_CONTROLLER* e712Controller = new PI_E712_CONTROLLER;
		e712Controller->controllerState = CONTROLLER_NOT_CONNECTED;
		e712Controller->lastError = 0;
		m_Controller.push_back(e712Controller);

		yat::Socket::init();
		yat::Address addr(conf[i].url, conf[i].port);
		e712Controller->controllerSocket.connect(addr);

		e712Controller->controllerSocket << "*IDN?\n";
		string answer;
		e712Controller->controllerSocket >> answer;
		cout << "e712 connected to \"" << answer << "\"" << endl;

		e712Controller->controllerSocket << "SAI?\n";
		e712Controller->controllerSocket >> answer;
		istringstream is(answer);
		string line;

		while(getline(is, line))
		{
			PI_E712_AXIS* e712Axis = new PI_E712_AXIS;
			e712Axis->axisState = AXIS_NOT_REFERENCED;
			e712Axis->axisName = atoi(line.c_str());
			e712Controller->controllerAxis.push_back(e712Axis);

			PI_SYSTEM_AXIS* controllerAxis = new PI_SYSTEM_AXIS;
			controllerAxis->idAxis = e712Controller->controllerAxis.size() - 1;
			controllerAxis->idController = m_Controller.size() - 1;
			m_controllerAxis.push_back(controllerAxis);
		}

		e712Controller->controllerState = CONTROLLER_CONNECTED;
		ReadStateFromE712(m_Controller.size() - 1);
		InitSocket(m_Controller.size() - 1);
	}
}

// ============================================================================
// Socket Disconnect
// ============================================================================
void PIE712::Disconnect()
{
	int i;
	int j;
	for(i = 0; i < (int)m_Controller.size(); i++)
	{
		for(j = 0; j < (int)m_Controller[i]->controllerAxis.size(); j++)
			delete m_Controller[i]->controllerAxis[j];

		m_Controller[i]->controllerAxis.clear();
		m_Controller[i]->controllerSocket.disconnect();
		m_Controller[i]->controllerState = CONTROLLER_NOT_CONNECTED;
		delete m_Controller[i];
	}
	m_Controller.clear();


	for(i = 0; i < (int)m_controllerAxis.size(); i++)
			delete m_controllerAxis[i];

	m_controllerAxis.clear();
}

// ============================================================================
// Get state of e712
// ============================================================================
PIE712::E712_CONTROLLER_STATE PIE712::GetStatus()
{
	YAT_TRACE("PIE712::GetStatus");
	ReadStateFromE712(0);

	return m_Controller[0]->controllerState;
}


// ============================================================================
// Get state of controller axis 'controllerAxisId'
// ============================================================================
PIE712::E712_AXIS_STATE PIE712::GetAxisStatusOfAxis(unsigned int controllerAxisId)
{
	YAT_TRACE("PIE712::GetAxisStatusOfAxis");

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	ReadStateFromE712(m_controllerAxis[controllerAxisId - 1]->idController);

	return m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisState;

}



// ============================================================================
// Get state of sall ystem axes 
// ============================================================================
void PIE712::GetAxisStatus(std::vector<E712_AXIS_STATE>& axisState)
{
	int i;
	YAT_TRACE("PIE712::GetAxisStatusOfAxis");

	for(i = 0; i < int(m_Controller.size()); i++)
		ReadStateFromE712(i);

	for(i = 0; i < int(m_controllerAxis.size()); i++)
		axisState.push_back(m_Controller[m_controllerAxis[i]->idController]->controllerAxis[m_controllerAxis[i]->idAxis]->axisState);
}


// ============================================================================
// Get the number of axis
// ============================================================================
int PIE712::GetNumberOfAxes()
{
	return m_controllerAxis.size();
}
// ============================================================================
// stop anything, reference, normal motion,..., for axis 'controllerAxisId'
// ============================================================================
void PIE712::StopAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	// Its not possible to stop a single axis at the E-712 controller with the STP command.
	// The obly way to simulate this is to read back the current possition of the axis,
	// and set this possition as the new target of the axis.
	MoveAbsoluteSingleAxis(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName, ReadSinglePosition(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName));
}

// ============================================================================
// stop anything, reference, normal motion,..., for all controller axes
// ============================================================================
void PIE712::Stop()
{
	char c = 24;
	unsigned int i;

	for(i = 0; i < m_Controller.size(); i++)
	{
		if (m_Controller[i]->controllerState != CONTROLLER_NOT_CONNECTED && m_Controller[i]->controllerState != CONTROLLER_CONNECTION_ERROR)
			m_Controller[i]->controllerSocket.send(&c, 1);
	}

	for(i = 0; i < m_Controller.size(); i++)
	{
		if (m_Controller[i]->controllerState != CONTROLLER_NOT_CONNECTED && m_Controller[i]->controllerState != CONTROLLER_CONNECTION_ERROR)
			ReadError(i);
	}
}

// ============================================================================
// set homing mode of axis 'controllerAxisId' to target position
// ============================================================================
void PIE712::SetHomingModeOfAxis(unsigned int controllerAxisId, E712_HOMING_MODE homingMode)
{
	int tmpCommandLevel;
	ostringstream cmd;

	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	tmpCommandLevel = GetCommandLevel(m_controllerAxis[controllerAxisId - 1]->idController);
	SetCommandLevel(m_controllerAxis[controllerAxisId - 1]->idController, 1);

	cmd << "SPA " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << " 0x02000a00 " <<  homingMode << "\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	cout << "E712: sent " << cmd.str();

	ReadStateFromE712(m_controllerAxis[controllerAxisId - 1]->idController);
	CheckE712ForError(m_controllerAxis[controllerAxisId - 1]->idController, "Could not set homing mode", __FUNCTION__);

	SetCommandLevel(m_controllerAxis[controllerAxisId - 1]->idController, tmpCommandLevel);
}

// ============================================================================
// set homing mode of all controller axes to target position
// ============================================================================
void PIE712::SetHomingMode(const std::vector<E712_HOMING_MODE>& homingMode)
{
	int tmpCommandLevel;
	unsigned int i, j, k;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	if (homingMode.size() != m_controllerAxis.size())
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of homing modes (must be 'number or controller axes')",
								__FUNCTION__ );
	}

	k = 0;
	for(i = 0; i < m_Controller.size(); i++)
	{
		tmpCommandLevel = GetCommandLevel(i);
		SetCommandLevel(i, 1);

		ostringstream cmd;
		cmd	<< "SPA";

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
		{
			cmd	<< " " << m_Controller[i]->controllerAxis[j]->axisName << " 0x02000a00 " << homingMode[k];
			k++;
		}

		cmd << "\n";

		m_Controller[i]->controllerSocket << cmd.str();
		cout << "E712: sent "<<cmd.str();

		ReadStateFromE712(i);
		CheckE712ForError(i, "Could not set homing mode", __FUNCTION__);

		SetCommandLevel(i, tmpCommandLevel);
	}
}

// ============================================================================
// set the homing position of axis 'controllerAxisId'
// ============================================================================
void PIE712::SetHomingPositionOfAxis(unsigned int controllerAxisId, double homingPosition)
{
	int tmpCommandLevel;
	ostringstream cmd;

	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	tmpCommandLevel = GetCommandLevel(m_controllerAxis[controllerAxisId - 1]->idController);
	SetCommandLevel(m_controllerAxis[controllerAxisId - 1]->idController, 1);

	cmd << "SPA " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << " 0x02000200 " <<  homingPosition << "\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	cout << "E712: sent " << cmd.str();

	ReadStateFromE712(m_controllerAxis[controllerAxisId - 1]->idController);
	CheckE712ForError(m_controllerAxis[controllerAxisId - 1]->idController, "Could not set homing position", __FUNCTION__);

	SetCommandLevel(m_controllerAxis[controllerAxisId - 1]->idController, tmpCommandLevel);
}

// ============================================================================
// set the homing position of all controller axes
// ============================================================================
void PIE712::SetHomingPosition(const std::vector<double>& homingPosition)
{
	int tmpCommandLevel;
	unsigned int i, j, k;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	if (homingPosition.size() != m_controllerAxis.size())
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of homing positions (must be 'number or controller axes')",
								__FUNCTION__ );
	}


	k = 0;
	for(i = 0; i < m_Controller.size(); i++)
	{
		tmpCommandLevel = GetCommandLevel(i);
		SetCommandLevel(i, 1);

		ostringstream cmd;
		cmd	<< "SPA";

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
		{
			cmd	<< " " << m_Controller[i]->controllerAxis[j]->axisName << " 0x02000200 " << homingPosition[k];
			k++;
		}

		cmd << "\n";

		m_Controller[i]->controllerSocket << cmd.str();
		cout << "E712: sent "<<cmd.str();

		ReadStateFromE712(i);
		CheckE712ForError(i, "Could not set homing position", __FUNCTION__);

		SetCommandLevel(i, tmpCommandLevel);
	}

}


// ============================================================================
// command Home axis 'controllerAxisId'
// ============================================================================
void PIE712::HomeAxis(unsigned int controllerAxisId)
{
	ostringstream cmd;

	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	if (m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisState != AXIS_NOT_REFERENCED)
		CheckBusy(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	cmd << "FRF " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << "\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	ReadStateFromE712(m_controllerAxis[controllerAxisId - 1]->idController);

	if (m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerState != CONTROLLER_REFERENCING)
	{
		ThrowE712Error(m_controllerAxis[controllerAxisId - 1]->idController, "Homing not started", __FUNCTION__);
	}
}

// ============================================================================
// command Home to all controller axes
// ============================================================================
void PIE712::Home()
{
	unsigned int i;

	for(i = 0; i < m_Controller.size(); i++)
	{
		CheckConnection(i, __FUNCTION__);

		if (m_Controller[i]->controllerState != CONTROLLER_NOT_REFERENCED)
			CheckBusy(i, __FUNCTION__);
	}

	for(i = 0; i < m_Controller.size(); i++)
	{
		m_Controller[i]->controllerSocket << "FRF\n";
		ReadStateFromE712(i);

		if (m_Controller[i]->controllerState != CONTROLLER_REFERENCING)
		{
			ThrowE712Error(i, "Homing not started", __FUNCTION__);
		}
	}
}

// ============================================================================
// Get current position of axis 'controllerAxisId'
// ============================================================================
double PIE712::GetPositionOfAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	return ReadSinglePosition(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName);
}

// ============================================================================
// get the current position of all axes
// ============================================================================
void PIE712::GetPosition(std::vector<double>& currentPosition)
{
	unsigned int i;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	currentPosition.clear();
	for(i = 0; i < m_Controller.size(); i++)
	{
		m_Controller[i]->controllerSocket << "POS?\n";
		ReadFloatValues(i, currentPosition);
		CheckE712ForError(i, "Could not get position", __FUNCTION__);
	}
}

// ============================================================================
// move axis 'controllerAxisId' to target position
// ============================================================================
void PIE712::MoveAbsoluteAxis(unsigned int controllerAxisId, double targetPosition)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);
	CheckBusy(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	MoveAbsoluteSingleAxis(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName, targetPosition);
}

// ============================================================================
// move all controller axes to target position 
// ============================================================================
void PIE712::MoveAbsolute(const std::vector<double>& targetPosition)
{
	unsigned int i, j, k;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	if (targetPosition.size() != m_controllerAxis.size())
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of target positions (must be 'number or controller axes')",
								__FUNCTION__ );
	}

	for(i = 0; i < m_Controller.size(); i++)
		CheckStandby(i, __FUNCTION__);


	k = 0;
	for(i = 0; i < m_Controller.size(); i++)
	{
		ostringstream cmd;
		cmd	<< "MOV";

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
		{
			cmd	<< " " << m_Controller[i]->controllerAxis[j]->axisName << " " << targetPosition[k];
			k++;
		}

		cmd << "\n";

		m_Controller[i]->controllerSocket << cmd.str();
		cout << "E712: sent "<<cmd.str();

		ReadStateFromE712(i);
		if (m_Controller[i]->controllerState != CONTROLLER_MOVING)
		{
			ThrowE712Error(i, "Motion not started", __FUNCTION__);
		}
	}
}

// ============================================================================
// move axis 'controllerAxisId' relative
// ============================================================================
void PIE712::MoveReletiveAxis(unsigned int controllerAxisId, double relativePosition)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);
	CheckBusy(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	MoveRelativeSingleAxis(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName, relativePosition);
}

// ============================================================================
// move all controller axes relative
// ============================================================================
void PIE712::MoveReletive(const std::vector<double>& relativePosition)
{
	unsigned int i, j, k;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	if (relativePosition.size() != m_controllerAxis.size())
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of relative positions (must be 'number or controller axes')",
								__FUNCTION__ );
	}

	for(i = 0; i < m_Controller.size(); i++)
		CheckStandby(i, __FUNCTION__);


	k = 0;
	for(i = 0; i < m_Controller.size(); i++)
	{
		ostringstream cmd;
		cmd	<< "MVR";

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
		{
			cmd	<< " " << m_Controller[i]->controllerAxis[j]->axisName << " " << relativePosition[k];
			k++;
		}

		cmd << "\n";

		m_Controller[i]->controllerSocket << cmd.str();
		cout << "E712: sent "<<cmd.str();

		ReadStateFromE712(i);
		if (m_Controller[i]->controllerState != CONTROLLER_MOVING)
		{
			ThrowE712Error(i, "Motion not started", __FUNCTION__);
		}
	}
}

// ============================================================================
// Get target position of axis 'controllerAxisId'
// ============================================================================
double PIE712::GetTargetOfAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	return ReadSingleTarget(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName);
}


// ============================================================================
// get target position of all controller axes
// ============================================================================
void PIE712::GetTarget(std::vector<double>& targetPotition)
{
	unsigned int i;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	targetPotition.clear();
	for(i = 0; i < m_Controller.size(); i++)
	{
		m_Controller[i]->controllerSocket << "MOV?\n";
		ReadFloatValues(i, targetPotition);
		CheckE712ForError(i, "Could not get target", __FUNCTION__);
	}
}

// ============================================================================
// Set velocity of axis 'controllerAxisId'
// ============================================================================
void PIE712::SetVelocityOfAxis(unsigned int controllerAxisId, double velocity)
{
	ostringstream cmd;

	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);
	CheckBusy(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	cmd << "VEL " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << " " <<  velocity << "\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	cout << "E712: sent " << cmd.str();

	ReadStateFromE712(m_controllerAxis[controllerAxisId - 1]->idController);
	CheckE712ForError(m_controllerAxis[controllerAxisId - 1]->idController, "Could not set velocity", __FUNCTION__);
}

// ============================================================================
// Set velocity of all controller axes
// ============================================================================
void PIE712::SetVelocity(const std::vector<double>& velocity)
{
	unsigned int i, j, k;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	if (velocity.size() != m_controllerAxis.size())
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of velocities (must be 'number or controller axes')",
								__FUNCTION__ );
	}

	for(i = 0; i < m_Controller.size(); i++)
		CheckStandby(i, __FUNCTION__);


	k = 0;
	for(i = 0; i < m_Controller.size(); i++)
	{
		ostringstream cmd;
		cmd	<< "VEL";

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
		{
			cmd	<< " " << m_Controller[i]->controllerAxis[j]->axisName << " " << velocity[k];
			k++;
		}

		cmd << "\n";

		m_Controller[i]->controllerSocket << cmd.str();
		cout << "E712: sent "<<cmd.str();

		ReadStateFromE712(i);
		CheckE712ForError(i, "Could not set velocity", __FUNCTION__);
	}
}

// ============================================================================
// Get velocity of axis 'controllerAxisId'
// ============================================================================
double PIE712::GetVelocityOfAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	return ReadSingleTarget(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName);
}

// ============================================================================
// get velocity of all controller axes
// ============================================================================
void PIE712::GetVelocity(std::vector<double>& velocity)
{
	unsigned int i;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	velocity.clear();
	for(i = 0; i < m_Controller.size(); i++)
	{
		m_Controller[i]->controllerSocket << "VEL?\n";
		ReadFloatValues(i, velocity);
		CheckE712ForError(i, "Could not get velocity", __FUNCTION__);
	}
}


// ============================================================================
// enable the servo for axis 'controllerAxisId'
// ============================================================================
void PIE712::EnableServoOfAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);
	CheckStandby(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	ostringstream cmd;
	cmd	<< "SVO" << " " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << " 1\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	cout << "E712: sent "<<cmd.str();

	ReadStateFromE712(m_controllerAxis[controllerAxisId - 1]->idController);
	CheckE712ForError(m_controllerAxis[controllerAxisId - 1]->idController, "Could not enable servo", __FUNCTION__);
}

// ============================================================================
// enable the servo for all controller axes
// ============================================================================
void PIE712::EnableServo()
{
	unsigned int i, j;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	for(i = 0; i < m_Controller.size(); i++)
		CheckStandby(i, __FUNCTION__);


	for(i = 0; i < m_Controller.size(); i++)
	{
		ostringstream cmd;
		cmd	<< "SVO";

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
			cmd	<< " " << m_Controller[i]->controllerAxis[j]->axisName << " 1";

		cmd << "\n";

		m_Controller[i]->controllerSocket << cmd.str();
		cout << "E712: sent "<<cmd.str();

		ReadStateFromE712(i);
		CheckE712ForError(i, "Could not enable servo", __FUNCTION__);
	}
}


// ============================================================================
// disable the servo for axis 'controllerAxisId'
// ============================================================================
void PIE712::DisableServoOfAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);
	CheckStandby(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	ostringstream cmd;
	cmd	<< "SVO" << " " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << " 0\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	cout << "E712: sent "<<cmd.str();

	ReadStateFromE712(m_controllerAxis[controllerAxisId - 1]->idController);
	CheckE712ForError(m_controllerAxis[controllerAxisId - 1]->idController, "Could not disable servo", __FUNCTION__);
}

// ============================================================================
// disable the servo for all controller axes
// ============================================================================
void PIE712::DisableServo()
{
	unsigned int i, j;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	for(i = 0; i < m_Controller.size(); i++)
		CheckStandby(i, __FUNCTION__);


	for(i = 0; i < m_Controller.size(); i++)
	{
		ostringstream cmd;
		cmd	<< "SVO";

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
			cmd	<< " " << m_Controller[i]->controllerAxis[j]->axisName << " 0";

		cmd << "\n";

		m_Controller[i]->controllerSocket << cmd.str();
		cout << "E712: sent "<<cmd.str();

		ReadStateFromE712(i);
		CheckE712ForError(i, "Could not disable servo", __FUNCTION__);
	}
}


// ============================================================================
// get current positive soft limit for axis 'controllerAxisId'
// ============================================================================
double PIE712::GetSoftPositiveLimitOfAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	return ReadSinglePositiveLimit(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName);
}


// ============================================================================
// get current negative soft limit for axis 'controllerAxisId'
// ============================================================================
double PIE712::GetSoftNegativeLimitOfAxis(unsigned int controllerAxisId)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	return ReadSingleNegativeLimit(m_controllerAxis[controllerAxisId - 1]->idController, m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName);
}


// ============================================================================
// get current soft limits for all controller axes
// ============================================================================
void PIE712::GetSoftLimits(std::vector<double>& positiveLimits, std::vector<double>& negativeLimits)
{
	unsigned int i, j;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	positiveLimits.clear();
	negativeLimits.clear();

	for(i = 0; i < m_Controller.size(); i++)
	{
		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
		{
			ostringstream cmdPositiveLimit;
			cmdPositiveLimit << "SPA? " << m_Controller[i]->controllerAxis[j]->axisName << " 0x07000001\n";
			m_Controller[i]->controllerSocket << cmdPositiveLimit.str();
			ReadSingleFloatValue(i, positiveLimits);

			ostringstream cmdNegativeLimit;
			cmdNegativeLimit << "SPA? " << m_Controller[i]->controllerAxis[j]->axisName << " 0x07000000\n";
			m_Controller[i]->controllerSocket << cmdNegativeLimit.str();
			ReadSingleFloatValue(i, negativeLimits);
		}
		CheckE712ForError(i, "Could not get soft limits", __FUNCTION__);
	}
}


// ============================================================================
// set current positive soft limit for axis 'controllerAxisId'
// ============================================================================
void PIE712::SetSoftPositiveLimitOfAxis(unsigned int controllerAxisId, double positiveLimits)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	ostringstream cmd;
	cmd	<< "SPA" << " " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << " 0x07000001 " << positiveLimits << "\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	cout << "E712: sent "<<cmd.str();

	CheckE712ForError(m_controllerAxis[controllerAxisId - 1]->idController, "Could not set positive limit", __FUNCTION__);
}


// ============================================================================
// set current negative soft limit for axis 'controllerAxisId'
// ============================================================================
void PIE712::SetSoftNegativeLimitOfAxis(unsigned int controllerAxisId, double negativeLimits)
{
	CheckConnection(m_controllerAxis[controllerAxisId - 1]->idController, __FUNCTION__);

	if((controllerAxisId < 1) || (controllerAxisId > m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid axis ID (must be between 1 and 'number or controller axes')",
								__FUNCTION__ );
	}

	ostringstream cmd;
	cmd	<< "SPA" << " " << m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerAxis[m_controllerAxis[controllerAxisId - 1]->idAxis]->axisName << " 0x07000000 " << negativeLimits << "\n";
	m_Controller[m_controllerAxis[controllerAxisId - 1]->idController]->controllerSocket << cmd.str();
	cout << "E712: sent "<<cmd.str();

	CheckE712ForError(m_controllerAxis[controllerAxisId - 1]->idController, "Could not set positive limit", __FUNCTION__);
}


// ============================================================================
// set current soft limits for all controller axes
// ============================================================================
void PIE712::SetSoftLimits(const std::vector<double>& positiveLimits, const std::vector<double>& negativeLimits)
{
	int tmpCommandLevel;
	unsigned int i, j, k;

	for(i = 0; i < m_Controller.size(); i++)
		CheckConnection(i, __FUNCTION__);

	if ((negativeLimits.size() != positiveLimits.size()) ||
		(negativeLimits.size() != m_controllerAxis.size()))
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of limits (must be 'number or controller axes')",
								__FUNCTION__ );
	}

	k = 0;
	for(i = 0; i < m_Controller.size(); i++)
	{
		tmpCommandLevel = GetCommandLevel(i);
		SetCommandLevel(i, 1);

		for(j = 0; j < m_Controller[i]->controllerAxis.size(); j++)
		{
			ostringstream cmdPositiveLimit;
			cmdPositiveLimit << "SPA " << m_Controller[i]->controllerAxis[j]->axisName << " 0x07000001 " <<  positiveLimits[k] << "\n";
			m_Controller[i]->controllerSocket << cmdPositiveLimit.str();

			ostringstream cmdNegativeLimit;
			cmdNegativeLimit << "SPA " << m_Controller[i]->controllerAxis[j]->axisName << " 0x07000000 " <<  negativeLimits[k] << "\n";
			m_Controller[i]->controllerSocket << cmdNegativeLimit.str();

			k++;
		}

		SetCommandLevel(i, tmpCommandLevel);
		CheckE712ForError(i, "Could not set limits", __FUNCTION__);
	}

}


// ============================================================================
// Send ASCII command (GCS) to controller must be terminated by LineFeed (\n)
// ============================================================================
void PIE712::SendGCSCommand(const std::string& command)
{
	if (m_Controller[0]->controllerState == CONTROLLER_NOT_CONNECTED || m_Controller[0]->controllerState == CONTROLLER_CONNECTION_ERROR)
	{
		YAT_LOG("PIE712::SendGCSCommand() NOT CONNECTED OR CONNECTION_ERROR"); 
		ThrowSoftwareError("E712 not connected", __FUNCTION__);
	}
	m_Controller[0]->controllerSocket << command;
}

// ============================================================================
// Send ASCII answer (GCS) from controller
// ============================================================================
std::string PIE712::ReadGCSAnswer()
{
	CheckConnection(0, __FUNCTION__);
	std::string answer;
	std::string line;
	// single lines in multiline answers will be terminated with " \n" (Space + LineFeed).
	// Only the last line will end with a linefeed without space.
	for(;;)
	{
		m_Controller[0]->controllerSocket >> line;
		answer += line;
		if (	(line.length()==1)
			&&	(line == "\n")		)
		{
			break;
		}
		if (	(line.length()>1)
			&&	(line[line.length()-1] == '\n')
			&&	(line[line.length()-2] != ' ')	)
		{
			break;
		}
	}
	return answer;
}

// ============================================================================
// Find out State of E712 - internal utility method
// ============================================================================
void PIE712::ReadStateFromE712(int iE712Id)
{
	YAT_TRACE("PIE712::ReadStateFromE712");
	//- protect against socket errors
	if (m_Controller[iE712Id]->controllerState == CONTROLLER_NOT_CONNECTED || m_Controller[iE712Id]->controllerState == CONTROLLER_CONNECTION_ERROR)
	{
		YAT_LOG("PIE712::ReadStateFromE712() NOT CONNECTED OR CONNECTION_ERROR"); 
		return;
	}

	m_Controller[iE712Id]->controllerState = CONTROLLER_NO_STATE;
	int i;
	for(i = 0; i < (int)m_Controller[iE712Id]->controllerAxis.size(); i++)
		m_Controller[iE712Id]->controllerAxis[i]->axisState = AXIS_NO_STATE;

	char c = 7;
	m_Controller[iE712Id]->controllerSocket.send(&c, 1);
	string answer;
	int bitmask = 0;
	m_Controller[iE712Id]->controllerSocket >> answer;
	istringstream isIsReferencing(answer);
	isIsReferencing >> bitmask;
	if((unsigned char)answer[0] == 0xb0)
	{
		m_Controller[iE712Id]->controllerState = CONTROLLER_REFERENCING;
	}


	std::vector<int> referenceStatus;
	m_Controller[iE712Id]->controllerSocket << "FRF?\n";
	ReadIntValues(iE712Id, referenceStatus);
	bool e712IsReferenced = true;
	for(i = 0; i < (int)m_Controller[iE712Id]->controllerAxis.size(); i++)
	{
		if(referenceStatus[i] == 0)
		{
			m_Controller[iE712Id]->controllerAxis[i]->axisState = AXIS_NOT_REFERENCED;
			e712IsReferenced = false;
		}
	}

	if((e712IsReferenced == false) && (m_Controller[iE712Id]->controllerState == CONTROLLER_NO_STATE))
		m_Controller[iE712Id]->controllerState = CONTROLLER_NOT_REFERENCED;


	bool e712IsMoving = false;
	c = 5;
	m_Controller[iE712Id]->controllerSocket.send(&c, 1);
	bitmask = 0;
	m_Controller[iE712Id]->controllerSocket >> answer;
	istringstream isIsMoving(answer);
	isIsMoving >> bitmask;
	for(i = 0; i < (int)m_Controller[iE712Id]->controllerAxis.size(); i++)
	{
		if((bitmask & (1 << i)) == (1 << i))
		{
			if(m_Controller[iE712Id]->controllerAxis[i]->axisState == AXIS_NO_STATE)
			{
				m_Controller[iE712Id]->controllerAxis[i]->axisState = AXIS_MOVING;
				e712IsMoving = true;
			}
		}
	}

	if((e712IsMoving == true) && (m_Controller[iE712Id]->controllerState == CONTROLLER_NO_STATE))
		m_Controller[iE712Id]->controllerState = CONTROLLER_MOVING;


	bool e712WaveGeneratorRunning = false;
	c = 9;
	m_Controller[iE712Id]->controllerSocket.send(&c, 1);
	bitmask = 0;
	m_Controller[iE712Id]->controllerSocket >> answer;
	istringstream isWaveGeneratorRunning(answer);
	isWaveGeneratorRunning >> bitmask;
	for(i = 0; i < (int)m_Controller[iE712Id]->controllerAxis.size(); i++)
	{
		if(m_Controller[iE712Id]->controllerAxis[i]->axisState == AXIS_NO_STATE)
		{
			if((bitmask & (1 << i))  == (1 << i))
			{
				m_Controller[iE712Id]->controllerAxis[i]->axisState = AXIS_WAVE_GENERATOR_RUNNING;
				e712WaveGeneratorRunning = true;
			}
		}
	}

	if((e712WaveGeneratorRunning == true) && (m_Controller[iE712Id]->controllerState == CONTROLLER_NO_STATE))
		m_Controller[iE712Id]->controllerState = CONTROLLER_WAVE_GENERATOR_RUNNING;


	m_Controller[iE712Id]->lastError = ReadError(iE712Id);
	if(m_Controller[iE712Id]->controllerState == CONTROLLER_NO_STATE)
	{
		if(m_Controller[iE712Id]->lastError == 1024)
			m_Controller[iE712Id]->controllerState = CONTROLLER_MOTION_ERROR;
		else if(m_Controller[iE712Id]->lastError != 0)
			m_Controller[iE712Id]->controllerState = CONTROLLER_HEXPOD_ERROR;
	}


	for(i = 0; i < (int)m_Controller[iE712Id]->controllerAxis.size(); i++)
		if(m_Controller[iE712Id]->controllerAxis[i]->axisState == AXIS_NO_STATE)
				m_Controller[iE712Id]->controllerAxis[i]->axisState = AXIS_STANDBY;

	if(m_Controller[iE712Id]->controllerState == CONTROLLER_NO_STATE)
		m_Controller[iE712Id]->controllerState = CONTROLLER_STANDBY;

	return;
}


// ============================================================================
// Get error code
// ============================================================================
int PIE712::ReadError(int iE712Id)
{
	int errorcode = 0;

	if(m_Controller[iE712Id]->lastError == 0)
	{
		m_Controller[iE712Id]->controllerSocket << "ERR?\n";
		string answer;
		m_Controller[iE712Id]->controllerSocket >> answer;
		istringstream is(answer);
		is >> errorcode;
	}
	else
	{
		errorcode = m_Controller[iE712Id]->lastError;
		m_Controller[iE712Id]->lastError = 0;
	}
	return errorcode;
}

// ============================================================================
// Utility : throw exception with e712 error code
// ============================================================================
void PIE712::ThrowE712Error(int iE712Id, const string& description, const string& funcname)
{
	int err = ReadError(iE712Id);
	ostringstream os;
	os << description << " - e712 reported error " <<err;
	throw yat::Exception( "E712_ERROR", os.str(), funcname );
}

// ============================================================================
//
// ============================================================================
void PIE712::CheckE712ForError(int iE712Id, const std::string& description, const std::string& funcname)
{
	int err = ReadError(iE712Id);
	if (0 == err)
		return;
	ostringstream os;
	os << description << " - e712 reported error " <<err;
	throw yat::Exception( "E712_ERROR", os.str(), funcname );
}

// ============================================================================
// Utility : throw exception - software error code
// ============================================================================
void PIE712::ThrowSoftwareError(const string& description, const string& funcname)
{
	throw yat::Exception( "SOFTWARE_ERROR", description, funcname );
}

// ============================================================================
// read on e712 the 'number of controller axes' positions in 1 shot
// ============================================================================
void PIE712::ReadFloatValues(int iE712Id, std::vector<double>& positions)
{
	string answer;
	do
	{
		m_Controller[iE712Id]->controllerSocket >> answer;
		istringstream is(answer);
		string line;
		double pos;
		while(getline(is, line))
		{
			size_t p = line.find('=');
			if (p == string::npos)
			{
				throw yat::Exception(	"SOFTWARE_ERROR",
										"unexpected response to position query",
										__FUNCTION__ );
			}
			istringstream spos(line.substr(p+1));
			spos >> pos;
			positions.push_back(pos);
		}
	} while (positions.size() < m_Controller[iE712Id]->controllerAxis.size());
}

// ============================================================================
// read  one positions 
// ============================================================================
void PIE712::ReadSingleFloatValue(int iE712Id, std::vector<double>& positions)
{
	string answer;
	m_Controller[iE712Id]->controllerSocket >> answer;
	istringstream is(answer);
	string line;
	double pos;
	while(getline(is, line))
	{
		size_t p = line.find('=');
		if (p == string::npos)
		{
			throw yat::Exception(	"SOFTWARE_ERROR",
									"unexpected response to position query",
									__FUNCTION__ );
		}
		istringstream spos(line.substr(p+1));
		spos >> pos;
		positions.push_back(pos);
	}
}


// ============================================================================
// utility: read target position of singel axis
// ============================================================================
double PIE712::ReadSingleTarget(int iE712Id, unsigned int controllerAxisName)
{
	ostringstream cmd;
	cmd	<< "MOV? " << controllerAxisName <<"\n";
	m_Controller[iE712Id]->controllerSocket << cmd.str();
	std::string answer;
	m_Controller[iE712Id]->controllerSocket >> answer;
	size_t p = answer.find('=');
	if (p == string::npos)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"unexpected response to query",
								__FUNCTION__ );
	}
	istringstream is(answer.substr(p+1));
	double pos;
	is >> pos;
	return pos;
}

// ============================================================================
// utility: read current position of singel axis
// ============================================================================
double PIE712::ReadSinglePosition(int iE712Id, unsigned int controllerAxisName)
{
	ostringstream cmd;
	cmd	<< "POS? " << controllerAxisName <<"\n";
	m_Controller[iE712Id]->controllerSocket << cmd.str();
	std::string answer;
	m_Controller[iE712Id]->controllerSocket >> answer;
	size_t p = answer.find('=');
	if (p == string::npos)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"unexpected response to query",
								__FUNCTION__ );
	}
	istringstream is(answer.substr(p+1));
	double pos;
	is >> pos;
	return pos;
}


// ============================================================================
// utility: read current positive limit of singel axis
// ============================================================================
double PIE712::ReadSinglePositiveLimit(int iE712Id, unsigned int controllerAxisName)
{
	ostringstream cmd;
	cmd	<< "SPA? " << controllerAxisName <<" 0x07000001\n";
	m_Controller[iE712Id]->controllerSocket << cmd.str();
	std::string answer;
	m_Controller[iE712Id]->controllerSocket >> answer;
	size_t p = answer.find('=');
	if (p == string::npos)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"unexpected response to query",
								__FUNCTION__ );
	}
	istringstream is(answer.substr(p+1));
	double pos;
	is >> pos;

	CheckE712ForError(iE712Id, "Could not read positive limit", __FUNCTION__);
	return pos;
}

// ============================================================================
// utility: read current negative limit of singel axis
// ============================================================================
double PIE712::ReadSingleNegativeLimit(int iE712Id, unsigned int controllerAxisName)
{
	ostringstream cmd;
	cmd	<< "SPA? " << controllerAxisName <<" 0x07000000\n";
	m_Controller[iE712Id]->controllerSocket << cmd.str();
	std::string answer;
	m_Controller[iE712Id]->controllerSocket >> answer;
	size_t p = answer.find('=');
	if (p == string::npos)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"unexpected response to query",
								__FUNCTION__ );
	}
	istringstream is(answer.substr(p+1));
	double pos;
	is >> pos;

	CheckE712ForError(iE712Id, "Could not read positive limit", __FUNCTION__);
	return pos;
}


// ============================================================================
// utility to read 'number of controller axes' integer values from e712
// ============================================================================
void PIE712::ReadIntValues(int iE712Id, std::vector<int>& values)
{
	string answer;
	do
	{
		m_Controller[iE712Id]->controllerSocket >> answer;
		istringstream is(answer);
		string line;
		int val;
		while(getline(is, line))
		{
			size_t p = line.find('=');
			if (p == string::npos)
			{
				throw yat::Exception(	"SOFTWARE_ERROR",
										"unexpected response to query",
										__FUNCTION__ );
			}
			istringstream spos(line.substr(p+1));
			spos >> val;
			values.push_back(val);
		}
	} while (values.size() < m_Controller[iE712Id]->controllerAxis.size());
}



// ============================================================================
// Get the command level of the controller
// ============================================================================
int PIE712::GetCommandLevel(int iE712Id)
{
	int commandLevel = 0;

	m_Controller[iE712Id]->controllerSocket << "CCL?\n";
	string answer;
	m_Controller[iE712Id]->controllerSocket >> answer;
	istringstream is(answer);
	is >> commandLevel;

	return commandLevel;
}


// ============================================================================
// Set the command level of the controller
// ============================================================================
void PIE712::SetCommandLevel(int iE712Id, int commandLevel)
{
	switch(commandLevel)
	{
		case 0:
			m_Controller[iE712Id]->controllerSocket << "CCL 0\n";
			break;
		case 1:
			m_Controller[iE712Id]->controllerSocket << "CCL 1 ADVANCED\n";
			break;
	}
}



// ============================================================================
// Move a singel axis to 'targetPosition'
// ============================================================================
void PIE712::MoveAbsoluteSingleAxis(int iE712Id, unsigned int controllerAxisName, double targetPosition)
{
	ostringstream cmd;

	cmd << "MOV " << controllerAxisName << " " <<  targetPosition << "\n";
	m_Controller[iE712Id]->controllerSocket << cmd.str();
	cout << "E712: sent " << cmd.str();

	ReadStateFromE712(iE712Id);
	if (m_Controller[iE712Id]->controllerState != CONTROLLER_MOVING)
	{
		ThrowE712Error(iE712Id, "Motion not started", __FUNCTION__);
	}
}


// ============================================================================
// Move a singel axis relative
// ============================================================================
void PIE712::MoveRelativeSingleAxis(int iE712Id, unsigned int controllerAxisName, double targetPosition)
{
	ostringstream cmd;

	cmd << "MVR " << controllerAxisName << " " <<  targetPosition << "\n";
	m_Controller[iE712Id]->controllerSocket << cmd.str();
	cout << "E712: sent " << cmd.str();

	ReadStateFromE712(iE712Id);
	if (m_Controller[iE712Id]->controllerState != CONTROLLER_MOVING)
	{
		ThrowE712Error(iE712Id, "Motion not started", __FUNCTION__);
	}
}
