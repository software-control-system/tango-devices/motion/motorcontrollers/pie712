//=============================================================================
//
// file :         PIE712AxisClass.h
//
// description :  Include for the PIE712AxisClass root class.
//                This class is the singleton class for
//                the PIE712Axis device class.
//                It contains all properties and methods which the 
//                PIE712Axis requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author: jean_coquet $
//
// $Revision: 1.2 $
// $Date: 2011-12-02 12:51:51 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/Motion/PIE712/src/PIE712AxisClass.h,v $
// $Log: not supported by cvs2svn $
// Revision 1.1  2011/12/01 12:36:35  olivierroux
// -  initial import #20978
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _PIE712AXISCLASS_H
#define _PIE712AXISCLASS_H

#include <tango.h>
#include <PIE712Axis.h>


namespace PIE712Axis_ns
{//=====================================
//	Define classes for attributes
//=====================================
class negativeLimitAttrib: public Tango::Attr
{
public:
	negativeLimitAttrib():Attr("negativeLimit", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~negativeLimitAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PIE712Axis *>(dev))->read_negativeLimit(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<PIE712Axis *>(dev))->write_negativeLimit(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PIE712Axis *>(dev))->is_negativeLimit_allowed(ty);}
};

class positiveLimitAttrib: public Tango::Attr
{
public:
	positiveLimitAttrib():Attr("positiveLimit", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~positiveLimitAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PIE712Axis *>(dev))->read_positiveLimit(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<PIE712Axis *>(dev))->write_positiveLimit(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PIE712Axis *>(dev))->is_positiveLimit_allowed(ty);}
};

class velocityAttrib: public Tango::Attr
{
public:
	velocityAttrib():Attr("velocity", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~velocityAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PIE712Axis *>(dev))->read_velocity(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<PIE712Axis *>(dev))->write_velocity(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PIE712Axis *>(dev))->is_velocity_allowed(ty);}
};

class positionAttrib: public Tango::Attr
{
public:
	positionAttrib():Attr("position", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~positionAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<PIE712Axis *>(dev))->read_position(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<PIE712Axis *>(dev))->write_position(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<PIE712Axis *>(dev))->is_position_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class EnableCmd : public Tango::Command
{
public:
	EnableCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	EnableCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~EnableCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIE712Axis *>(dev))->is_Enable_allowed(any);}
};



class DisableClass : public Tango::Command
{
public:
	DisableClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	DisableClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~DisableClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIE712Axis *>(dev))->is_Disable_allowed(any);}
};



class InitializeReferencePositionClass : public Tango::Command
{
public:
	InitializeReferencePositionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	InitializeReferencePositionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~InitializeReferencePositionClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIE712Axis *>(dev))->is_InitializeReferencePosition_allowed(any);}
};



class DefinePositionClass : public Tango::Command
{
public:
	DefinePositionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	DefinePositionClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~DefinePositionClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIE712Axis *>(dev))->is_DefinePosition_allowed(any);}
};



class AbortClass : public Tango::Command
{
public:
	AbortClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	AbortClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~AbortClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIE712Axis *>(dev))->is_Abort_allowed(any);}
};



class StopClass : public Tango::Command
{
public:
	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StopClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<PIE712Axis *>(dev))->is_Stop_allowed(any);}
};



//
// The PIE712AxisClass singleton definition
//

class
#ifdef WIN32
	__declspec(dllexport)
#endif
	PIE712AxisClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static PIE712AxisClass *init(const char *);
	static PIE712AxisClass *instance();
	~PIE712AxisClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	PIE712AxisClass(string &);
	static PIE712AxisClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace PIE712Axis_ns

#endif // _PIE712AXISCLASS_H
